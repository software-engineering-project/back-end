const express = require("express");
const SDC = require("statsd-client");
const morgan = require("morgan");
const helmet = require("helmet");
const cors = require("cors");

const medium = require("./api/v1/routes/medium");
const mediatype = require("./api/v1/routes/mediatype");
const department = require("./api/v1/routes/department");
const pressRelease = require('./api/v1/routes/press_release')
const pressReleaseType = require('./api/v1/routes/press_release_type')
const pressReleaseTheme = require('./api/v1/routes/press_release_theme')
const article = require('./api/v1/routes/article')
const articleTheme = require('./api/v1/routes/article_theme')

const app = express();
const port = process.env.PORT || 3000;

const sdc = new SDC({
  host: "localhost",
  port: 8125,
});

app.set("json replacer", (_, v) => {
  return v ?? undefined;
});

app.use(helmet());
app.use(cors());
app.use(
  sdc.helpers.getExpressMiddleware(
    require("os").hostname() + `.${process.env.APP_NAME}`
  )
);
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/medium", medium);
app.use("/medium_type", mediatype);
app.use("/department", department);
app.use('/press_release', pressRelease);
app.use('/press_release_type', pressReleaseType);
app.use('/press_release_theme', pressReleaseTheme);
app.use('/article_theme', articleTheme);
app.use('/article', article)

app.listen(port, () => {
  console.log(`Listening on http://localhost:${port}`);
});
