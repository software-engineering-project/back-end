const { DataTypes, Model, Sequelize } = require('sequelize');

class Article extends Model {}

module.exports = function(sequelize) {
    Article.init({
        _id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
        },
        title: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        date: {
            type: DataTypes.DATEONLY,
            allowNull: false,
            defaultValue: Sequelize.fn('now')
        },
        tone: {
            type: DataTypes.ENUM('positive', 'negative'),
            allowNull: false
        },
        description: {
            type: DataTypes.TEXT,
        },
    }, {
        sequelize, 
        modelName: "Article"
    })
}