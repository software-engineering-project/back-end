module.exports = (sequelize) => {
  const Department = sequelize.models.Department;
  const Media = sequelize.models.Media;
  const Mediatype = sequelize.models.Mediatype;
  const PressRelease = sequelize.models.PressRelease;
  const PressReleaseTheme = sequelize.models.PressReleaseTheme;
  const PressReleaseType = sequelize.models.PressReleaseType;
  const Article = sequelize.models.Article;
  const ArticleTheme = sequelize.models.ArticleTheme;  

  Department.belongsToMany(Media, {
    through: "DepartmentMedia",
    onDelete: "RESTRICT"
  });
  Media.belongsToMany(Department, {
    through: "DepartmentMedia",
    onDelete: "CASCADE"
  });

  Mediatype.hasMany(Media, { onDelete: "RESTRICT" });
  Media.belongsTo(Mediatype);

  PressRelease.belongsToMany(PressReleaseTheme, {
    through: "PRThemePressRelease",
    onDelete: "CASCADE"
  });
  PressReleaseTheme.belongsToMany(PressRelease, {
    through: "PRThemePressRelease",
    onDelete: "RESTRICT"
  });

  PressReleaseType.hasMany(PressRelease, { onDelete: "RESTRICT" });
  PressRelease.belongsTo(PressReleaseType);

  Article.belongsToMany(ArticleTheme, {
    through: "ArticleThemeArticle",
    onDelete: "CASCADE"
  });
  ArticleTheme.belongsToMany(Article, {
    through: "ArticleThemeArticle",
    onDelete: "RESTRICT"
  });

  PressRelease.hasMany(Article, { onDelete: "RESTRICT" });
  Article.belongsTo(PressRelease, { onDelete: "RESTRICT" });

  return { Department, Media, Mediatype, PressRelease, PressReleaseTheme, PressReleaseType, Article, ArticleTheme };
};
