const { DataTypes, Model } = require("sequelize");

class Department extends Model {}

module.exports = function (sequelize) {
  Department.init(
    {
      _id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      value: {
        type: DataTypes.TEXT,
        allowNull: false,
        unique: true,
      },
    },
    {
      sequelize,
      modelName: "Department",
    }
  );
};
