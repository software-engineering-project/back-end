const { DataTypes, Model } = require('sequelize');

class PRTheme extends Model {}

module.exports = function(sequelize) {
    PRTheme.init(
        {
            _id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true,
              },
            value: {
                type: DataTypes.TEXT,
                allowNull: false,
                unique: true,
              },
        }, {
            sequelize,
            modelName: "PressReleaseTheme"
        }
    )
}