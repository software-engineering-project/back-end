const { DataTypes, Model } = require("sequelize");

class Mediatype extends Model {}

module.exports = function (sequelize) {
  Mediatype.init(
    {
      _id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      value: {
        type: DataTypes.TEXT,
        allowNull: false,
        unique: true,
      },
    },
    {
      sequelize,
      modelName: "Mediatype",
    }
  );
};
