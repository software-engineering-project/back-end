const { DataTypes, Model, Sequelize } = require('sequelize');

class PressRelease extends Model {}

module.exports = function(sequelize) {
    PressRelease.init({
        _id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
        },
        title: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        date: {
            type: DataTypes.DATEONLY,
            allowNull: false,
            defaultValue: Sequelize.fn('now')
        },
        description: {
            type: DataTypes.TEXT,
        },
    }, {
        sequelize, 
        modelName: "PressRelease"
    })
}