// const { Sequelize } = require("sequelize/types");

module.exports = async (sequelize) => {
  const Department = sequelize.models.Department;
  const Media = sequelize.models.Media;
  const Mediatype = sequelize.models.Mediatype;

  const [_64, _65, _40] = await Department.bulkCreate([
    { value: "64" },
    { value: "65" },
    { value: "40" },
  ]);

  const [radio, pqr, tv, web] = await Mediatype.bulkCreate([
    { value: "radio" },
    { value: "pqr" },
    { value: "tv" },
    { value: "web" },
  ]);

  Media.bulkCreate([
    { name: "Atomic Radio 40" },
    { name: "Chérie FM 40" },
    { name: "France Bleu Gascogne" },
    { name: "Frequence Grands Lacs" },
    { name: "Radio 100% 40" },
    { name: "Radio Mont de Matsan" },
    { name: "RFM 40" },
    { name: "Virgin Radio 40" },
    { name: "Nostalgie 40" },
  ]).then((media) => {
    media.forEach((medium) => {
      medium.setDepartments([_40]);
      medium.setMediatype(radio);
    });
  });

  Media.bulkCreate([
    { name: "Atomic Radio 64" },
    { name: "CANAL VASCO" },
    { name: "Chérie FM 64" },
    { name: "France Bleu Béarn" },
    { name: "France Bleu Pays Basque" },
    { name: "RADIO  Gure Iratia" },
    { name: "Radio 100% 64" },
    { name: "RADIO EITB " },
    { name: "Radio Inside 64" },
    { name: "RADIO LAPURDI" },
    { name: "RADIO MENDILLA" },
    { name: "Radio Oloron" },
    { name: "Radio voix du Béarn" },
    { name: "RFM 64" },
    { name: "Virgin Radio 64" },
    { name: "XIBEROKO BOTXA" },
    { name: "Nostalgie 64" },
  ]).then((media) => {
    media.forEach((medium) => {
      medium.setDepartments([_64]);
      medium.setMediatype(radio);
    });
  });

  Media.bulkCreate([
    { name: "Atomic Radio 65 " },
    { name: "Chérie FM 65" },
    { name: "Radio 100% 65" },
    { name: "Radio Inside 65" },
    { name: "Radio Présence" },
    { name: "Radio Vallee d'Aure" },
    { name: "RFM 65" },
    { name: "Virgin Radio 65" },
    { name: "Nostalgie 65" },
  ]).then((media) => {
    media.forEach((medium) => {
      medium.setDepartments([_65]);
      medium.setMediatype(radio);
    });
  });

  Media.bulkCreate([
    { name: "L'Avenir Agricole & Viticole Aquitain 40" },
    { name: "Les annonces Landaises" },
    { name: "Sud Ouest Landes" },
  ]).then((media) => {
    media.forEach((medium) => {
      medium.setDepartments([_40]);
      medium.setMediatype(pqr);
    });
  });

  Media.bulkCreate([
    { name: "La République des Pyrénées" },
    { name: "L'Avenir Agricole & Viticole Aquitain 64" },
    { name: "Le Journal de Saint Palais" },
    { name: "Le Miroir de la Soule" },
    { name: "Le Sillon" },
    { name: "L'Eclair des Pyrénées" },
    { name: "Mediabask" },
    { name: "Sud-Ouest Béarn" },
    { name: "Sud-Ouest Pays Basque" },
  ]).then((media) => {
    media.forEach((medium) => {
      medium.setDepartments([_64]);
      medium.setMediatype(pqr);
    });
  });

  Media.bulkCreate([
    { name: "Bulletin Agricole des Hautes-Pyrénées"},
    { name: "La Dépêche "},
    { name: "La Gazette du Midi"},
    { name: "La Nouvelle République des Pyrénées"},
    { name: "La semaine des Pyrénées"},
    { name: "Le Petit Journal 65"},
    { name: "L'Essor Bigourdan"},
  ]).then((media) => {
    media.forEach((medium) => {
      medium.setDepartments([_65]);
      medium.setMediatype(pqr);
    });
  });

  Media.bulkCreate([
    {name: "France 2 40"},
    {name: "France 3 40"},
    {name: "TV PI 40"},
    {name: "web tv landes"},
  ]).then((media) => {
    media.forEach((medium) => {
      medium.setDepartments([_40]);
      medium.setMediatype(tv);
    });
  });

  Media.bulkCreate([
    {name: "France 2 64"},
    {name: "France 3 64"},
    {name: "TV PI 64"},
  ]).then((media) => {
    media.forEach((medium) => {
      medium.setDepartments([_64]);
      medium.setMediatype(tv);
    });
  });

  Media.bulkCreate([
    {name: "France 2 65"},
    {name: "France 3 65"},
  ]).then((media) => {
    media.forEach((medium) => {
      medium.setDepartments([_65]);
      medium.setMediatype(tv);
    });
  });

  Media.bulkCreate([
    {name: "Annonces Landaises", link: "annonces-landaises.com"},
    {name: "Aquitainefr 40", link: "aqui.fr"},
    {name: "Atomic Radio 40", link: "http://www.atomicradio.fr"},
    {name: "Avenir Aquitaine 40", link: "www.avenir-aquitain.com"},
    {name: "France bleu gascogne", link: "www.francebleu.fr/gascogne"},
    {name: "Frequence Grand Lac", link: "www.frequencegrandslacs.fr"},
    {name: "HpYTV 40", link: "HpYTV la télé des pyrénées"},
    {name: "Le courrier français des Landes ", link: "Le Courrier Français des Landes"},
    {name: "objectif aquitaine 40", link: "objectifaquitaine.latribune.fr"},
    {name: "PressLib 40", link: "PressLib"},
    {name: "Radio 100% 40", link: "radio 100%"},
    {name: "Radio 100% 65", link: "radio 100%"},
    {name: "Radio mont de Marsan", link: "https://www.radio-mdm.fr/"},
    {name: "Sud Ouest 40", link: "www.sudouest.fr"},
  ]).then((media) => {
    media.forEach((medium) => {
      medium.setDepartments([_40]);
      medium.setMediatype(web);
    });
  });

  Media.bulkCreate([
    {name: "Aquitainefr 64", link: "aqui.fr"},
    {name: "Atomic Radio 64", link: "http://www.atomicradio.fr"},
    {name: "Avenir Aquitaine 64", link: "www.avenir-aquitain.com"},
    {name: "Entreprises engagées 64", link: "https://entreprisesengagees64.info/"},
    {name: "Flash-Infos Nouvelle-Aquitaine Occitanie 64", link: "http://www.flash-infos.com/information-economique-sud-ouest/"},
    {name: "France bleu Bearn", link: "https://www.francebleu.fr/bearn"},
    {name: "France bleu pays pasque", link: "www.francebleu.fr/pays-basque"},
    {name: "HpYTV 64", link: "HpYTV la télé des pyrénées"},
    {name: "La Republique des pyrenees", link: "www.larepubliquedespyrenees.fr"},
    {name: "La semaine du Pays Basque", link: "www.la-semaine-du-pays-basque.com"},
    {name: "Le Sillon", link: "www.lesillon.info"},
    {name: "Mediabask", link: "Mediabask.fr"},
    {name: "objectif aquitaine 64", link: "objectifaquitaine.latribune.fr"},
    {name: "PressLib 64", link: "PressLib"},
    {name: "Sud Ouest 64", link: "www.sudouest.fr"},
    {name: "WebTV oloron", link: "WEB TV OLORON"},
    
  ]).then((media) => {
    media.forEach((medium) => {
      medium.setDepartments([_64]);
      medium.setMediatype(web);
    });
  });

  Media.bulkCreate([
    {name: "Aquitainefr 65", link: "aqui.fr"},
    {name: "Atomic Radio 65", link: "http://www.atomicradio.fr"},
    {name: "Flash-Infos Nouvelle-Aquitaine Occitanie 65", link: "http://www.flash-infos.com/information-economique-sud-ouest/"},
    {name: "HpYTV 65 ", link: "HpYTV la télé des pyrénées"},
    {name: "La Depeche", link: "la depeche .fr"},
    {name: "La semaine des Pyrénées", link: "www.lasemainedespyrenees.fr"},
    {name: "Le petit journal ", link: "www.lepetitjournal.net"},
    {name: "Nouvelle Republique pyrenees", link: "www.nrpyrenees.fr"},
    {name: "PressLib 65", link: "PressLib"},
    {name: "Radio 100% 64", link: "radio 100%"},
    {name: "Radio presence", link: "www.radiopresence.com"},
    {name: "Tarbes/Lourdes infos", link: "Tarbes/lourdes…info"},
  ]).then((media) => {
    media.forEach((medium) => {
      medium.setDepartments([_65]);
      medium.setMediatype(web);
    });
  });
};













































