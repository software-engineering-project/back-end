const Router = require("express").Router;

const router = Router();

const {
    PressReleaseController
} = require('../../../controllers/press_release');

router.get('/', (req, res) => {
    PressReleaseController.findAll()
    .then((pressReleases) => {
        res.json(pressReleases)
    })
    .catch((error) => {
        res.status(404).json(error.message)
    })
})

router.get("/:id", (req, res) => {
  PressReleaseController.findById(req.params.id)
    .then((medium) => res.json(medium))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.post("/", (req, res) => {
  PressReleaseController.add(req.body)
    .then((medium) => res.json(medium))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.post('/:id/link', (req, res) => {
    PressReleaseController.link(req.params.id, req.body.articles)
    .then((medium) => res.json(medium))
    .catch((error) => res.status(404).send({ error: error.message }));
})

router.post('/:id/unlink', (req, res) => {
    PressReleaseController.unlink(req.params.id, req.body.articles)
    .then((medium) => res.json(medium))
    .catch((error) => res.status(404).send({ error: error.message }));
})

router.put("/:id", (req, res) => {
  PressReleaseController.set(req.params.id, req.body)
    .then((medium) => res.json(medium))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.delete("/:id", (req, res) => {
  PressReleaseController.remove(req.params.id)
    .then((medium) => res.json(medium))
    .catch((error) => res.status(404).send({ error: error.message }));
});

module.exports = router