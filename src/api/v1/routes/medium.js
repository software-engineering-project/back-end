const Router = require("express").Router;
const { MediaController } = require("../../../controllers/medium");

const router = Router();

router.get("/", (req, res) => {
  MediaController.findAll()
    .then((media) => res.json(media))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.get("/:id", (req, res) => {
  MediaController.findById(req.params.id)
    .then((medium) => res.json(medium))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.post("/", (req, res) => {
  MediaController.add(req.body)
    .then((medium) => res.json(medium))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.put("/:id", (req, res) => {
  MediaController.set(req.params.id, req.body)
    .then((medium) => res.json(medium))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.delete("/:id", (req, res) => {
  MediaController.remove(req.params.id)
    .then((medium) => res.json(medium))
    .catch((error) => res.status(404).send({ error: error.message }));
});

module.exports = router;
