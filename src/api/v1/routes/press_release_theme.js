const Router = require("express").Router;

const router = Router();

const {
  PressReleaseThemeController,
} = require("../../../controllers/press_release_theme");

router.get("/", (req, res) => {
  PressReleaseThemeController.findAll()
    .then((pressReleaseThemes) =>
      res.send(pressReleaseThemes.map((pressReleaseTheme) => pressReleaseTheme.toJSON()))
    )
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.get("/:id", (req, res) => {
  PressReleaseThemeController.findById(req.params.id)
    .then((pressReleaseTheme) => res.send(pressReleaseTheme.toJSON()))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.post("/", (req, res) => {
  PressReleaseThemeController.add(req.body)
    .then((pressReleaseTheme) => res.send(pressReleaseTheme.toJSON()))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.put("/:id", (req, res) => {
  PressReleaseThemeController.set(req.params.id, req.body)
    .then(([_, pressReleaseThemes]) =>
      res.send(pressReleaseThemes.map((pressReleaseTheme) => pressReleaseTheme.toJSON())[0])
    )
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.delete("/:id", (req, res) => {
  PressReleaseThemeController.remove(req.params.id)
    .then((pressReleaseTheme) => res.send(pressReleaseTheme.toJSON()))
    .catch((error) => res.status(404).send({ error: error.message }));
});

module.exports = router;
