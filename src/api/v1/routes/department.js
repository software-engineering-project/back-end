const Router = require("express").Router;

const router = Router();

const {
  DepartmentController,
} = require("../../../controllers/department");

router.get("/", (req, res) => {
  DepartmentController.findAll()
    .then((departments) =>
      res.send(departments.map((department) => department.toJSON()))
    )
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.get("/:id", (req, res) => {
  DepartmentController.findById(req.params.id)
    .then((department) => res.send(department.toJSON()))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.post("/", (req, res) => {
  DepartmentController.add(req.body)
    .then((department) => res.send(department.toJSON()))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.put("/:id", (req, res) => {
  DepartmentController.set(req.params.id, req.body)
    .then(([_, departments]) =>
      res.send(departments.map((department) => department.toJSON())[0])
    )
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.delete("/:id", (req, res) => {
  DepartmentController.remove(req.params.id)
    .then((department) => res.send(department.toJSON()))
    .catch((error) => res.status(404).send({ error: error.message }));
});

module.exports = router;
