const Router = require("express").Router;
const sequelize = require("../../../db");

const router = Router();

const {
  MediatypeController,
} = require("../../../controllers/mediatype");

router.get("/", (req, res) => {
  MediatypeController.findAll()
    .then((mediatypes) =>
      res.send(mediatypes.map((mediatype) => mediatype.toJSON()))
    )
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.get("/:id", (req, res) => {
  MediatypeController.findById(req.params.id)
    .then((mediatype) => res.send(mediatype.toJSON()))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.post("/", (req, res) => {
  MediatypeController.add(req.body)
    .then((mediatype) => res.send(mediatype.toJSON()))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.put("/:id", (req, res) => {
  MediatypeController.set(req.params.id, req.body)
    .then(([_, mediatypes]) =>
      res.send(mediatypes.map((mediatype) => mediatype.toJSON())[0])
    )
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.delete("/:id", (req, res) => {
  MediatypeController.remove(req.params.id)
    .then((mediatype) => res.send(mediatype.toJSON()))
    .catch((error) => res.status(404).send({ error: error.message }));
});

module.exports = router;
