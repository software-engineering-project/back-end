const Router = require("express").Router;

const router = Router();

const {
  PressReleaseTypeController,
} = require("../../../controllers/press_release_type");

router.get("/", (req, res) => {
  PressReleaseTypeController.findAll()
    .then((pressReleaseTypes) =>
      res.send(pressReleaseTypes.map((pressReleaseType) => pressReleaseType.toJSON()))
    )
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.get("/:id", (req, res) => {
  PressReleaseTypeController.findById(req.params.id)
    .then((pressReleaseType) => res.send(pressReleaseType.toJSON()))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.post("/", (req, res) => {
  PressReleaseTypeController.add(req.body)
    .then((pressReleaseType) => res.send(pressReleaseType.toJSON()))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.put("/:id", (req, res) => {
  PressReleaseTypeController.set(req.params.id, req.body)
    .then(([_, pressReleaseTypes]) =>
      res.send(pressReleaseTypes.map((pressReleaseType) => pressReleaseType.toJSON())[0])
    )
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.delete("/:id", (req, res) => {
  PressReleaseTypeController.remove(req.params.id)
    .then((pressReleaseType) => res.send(pressReleaseType.toJSON()))
    .catch((error) => res.status(404).send({ error: error.message }));
});

module.exports = router;
