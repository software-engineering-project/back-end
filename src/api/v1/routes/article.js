const Router = require("express").Router;

const router = Router();

const {
    ArticleController
} = require('../../../controllers/article');

router.get('/', (req, res) => {
    ArticleController.findAll()
    .then((pressReleases) => {
        res.json(pressReleases)
    })
    .catch((error) => {
        res.status(404).json(error.message)
    })
})

router.get("/:id", (req, res) => {
  ArticleController.findById(req.params.id)
    .then((medium) => res.json(medium))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.post("/", (req, res) => {
  ArticleController.add(req.body)
    .then((medium) => res.json(medium))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.post('/:id/link', (req, res) => {
    ArticleController.link(req.params.id, req.body.press_release)
    .then((medium) => res.json(medium))
    .catch((error) => res.status(404).send({ error: error.message }));
})

router.post('/:id/unlink', (req, res) => {
    ArticleController.unlink(req.params.id)
    .then((medium) => res.json(medium))
    .catch((error) => res.status(404).send({ error: error.message }));
})

router.put("/:id", (req, res) => {
  ArticleController.set(req.params.id, req.body)
    .then((medium) => res.json(medium))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.delete("/:id", (req, res) => {
  ArticleController.remove(req.params.id)
    .then((medium) => res.json(medium))
    .catch((error) => res.status(404).send({ error: error.message }));
});

module.exports = router;