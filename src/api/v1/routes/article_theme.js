const Router = require("express").Router;

const router = Router();

const {
  ArticleThemeController,
} = require("../../../controllers/article_theme");

router.get("/", (req, res) => {
  ArticleThemeController.findAll()
    .then((articleThemes) =>
      res.send(articleThemes.map((articleTheme) => articleTheme.toJSON()))
    )
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.get("/:id", (req, res) => {
  ArticleThemeController.findById(req.params.id)
    .then((articleTheme) => res.send(articleTheme.toJSON()))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.post("/", (req, res) => {
  ArticleThemeController.add(req.body)
    .then((articleTheme) => res.send(articleTheme.toJSON()))
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.put("/:id", (req, res) => {
  ArticleThemeController.set(req.params.id, req.body)
    .then(([_, articleThemes]) =>
      res.send(articleThemes.map((articleTheme) => articleTheme.toJSON())[0])
    )
    .catch((error) => res.status(404).send({ error: error.message }));
});

router.delete("/:id", (req, res) => {
  ArticleThemeController.remove(req.params.id)
    .then((articleTheme) => res.send(articleTheme.toJSON()))
    .catch((error) => res.status(404).send({ error: error.message }));
});

module.exports = router;
