module.exports.buildMediumObject = (medium) => {
  return {
    _id: medium._id,
    name: medium.name,
    link: medium.link,
    description: medium.description,
    departments: medium.Departments.map(({value}) => value),
    type: medium.Mediatype.value,
  };
};
