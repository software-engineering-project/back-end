module.exports.buildArticleObject = (article) => {
    return {
        _id: article._id,
        title: article.title,
        description: article.description ?? undefined,
        date: article.date,
        themes: article.ArticleThemes.map(({value}) => value),
        press_release: {
                _id: article.PressRelease?._id,
                title: article.PressRelease?.title,
                description: article.PressRelease?.description ?? undefined,
                date: article.PressRelease?.date,
                themes: article.PressRelease?.PressReleaseThemes.map(({ value }) => value),
                type: article.PressRelease?.PressReleaseType.value,
        }
    };
  };