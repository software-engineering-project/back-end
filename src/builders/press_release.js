const { buildArticleObject } = require("./article");

module.exports.buildPressReleaseObject = (pressRelease) => {
  return {
    _id: pressRelease._id,
    title: pressRelease.title,
    description: pressRelease.description ?? undefined,
    date: pressRelease.date,
    themes: pressRelease.PressReleaseThemes.map(({ value }) => value),
    type: pressRelease.PressReleaseType.value,
    articles:
      pressRelease.Articles?.map(({ _id, title, date, tone, ArticleThemes }) => {
        return {
          _id,
          title,
          date,
          tone,
          theme: ArticleThemes.map(({ value }) => value),
        };
      }) ?? undefined,
  };
};
