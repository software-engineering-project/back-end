const { Sequelize } = require("sequelize");
const departments = require("./models/department");
const medias = require("./models/medium");
const mediatypes = require("./models/mediatype");
const articles = require("./models/article");
const articleThemes = require("./models/article_theme");
const pressRelease = require("./models/press_release");
const pressReleaseTypes = require("./models/press_release_type");
const pressReleaseThemes = require("./models/press_release_theme");

const sequelize = new Sequelize({
  dialect: "postgres",
  database: process.env.POSTGRESQL_ADDON_DB,
  host: process.env.POSTGRESQL_ADDON_HOST,
  password: process.env.POSTGRESQL_ADDON_PASSWORD,
  port: parseInt(process.env.POSTGRESQL_ADDON_PORT ?? "5432", 10),
  username: process.env.POSTGRESQL_ADDON_USER,
  pool: {
    max: 3,
  },
});

sequelize
  .authenticate()
  .then((_) => {
    console.log("Connection has been established succesfuly");
  })
  .catch((error) => {
    console.error("Unable to connect to the database", error);
  });

departments(sequelize);
medias(sequelize);
mediatypes(sequelize);
articles(sequelize);
articleThemes(sequelize);
pressRelease(sequelize);
pressReleaseTypes(sequelize);
pressReleaseThemes(sequelize);

require("./models/associations")(sequelize);

(async () => {
    if (process.env.INIT_DATABASE === "true") {
      sequelize.sync({force: true}).then(() => {
      require("./init")(sequelize);
    })
  } else {
    await sequelize.sync()
  }
})();

module.exports = sequelize;
