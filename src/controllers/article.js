const { buildArticleObject } = require("../builders/article");
const sequelize = require("../db");

const PressRelease = sequelize.models.PressRelease;
const PressReleaseType = sequelize.models.PressReleaseType;
const PressReleaseTheme = sequelize.models.PressReleaseTheme;
const Article = sequelize.models.Article;
const ArticleTheme = sequelize.models.ArticleTheme;


const ArticleFindingOptions = {
    include: [
        {
          model: ArticleTheme,
          through: { attributes: [] },
          attributes: ["_id", "value"],
        },
        {
            model: PressRelease,
            include: [
              {
                model: PressReleaseTheme,
                through: { attributes: [] },
                attributes: ["_id", "value"],
              },
              {
                model: PressReleaseType,
                attributes: ["_id", "value"],
              },
            ],
        }
      ],
      attributes: {
        exclude: ['createdAt', 'updatedAt']
    }
}
module.exports.ArticleController = class ArticleController {
    static async findAll() {
        return Article.findAll(ArticleFindingOptions)
        .then((articles) => {
            return articles.map(buildArticleObject)
        })
    }

    static async findById(id) {
        return Article.findByPk(id, ArticleFindingOptions)
        .then(buildArticleObject)
    }

    static async add(body) {
        const { themes, pressReleaseId } = body;

        const transaction = await sequelize.transaction();

        try {
            const themesArticle = await ArticleTheme.findAndCountAll({ where: { value: themes }});

            if(
                (themesArticle.count == themes.length)
            ) {
                const toAdd = await Article.create({
                    title: body.title,
                    tone: body.tone,
                    date: body.date,
                    description: body.description
                }, {
                    transaction
                })

                await toAdd.setArticleThemes(themesArticle.rows, {transaction});

                if( pressReleaseId != undefined || pressReleaseId != null) {
                  const pressRelease = await PressRelease.findOne({ where: { _id: pressReleaseId } });
                  await toAdd.setPressRelease(pressRelease, {transaction});
                }

                await transaction.commit()

                return this.findById(toAdd._id);
            } else {
                throw Error("invalid themes or press release ID")
            }
        } catch(e) {
            await transaction.rollback();
            throw e;
        }
    }

    static async link(id, pressReleaseId) {
        const pressRelease = await PressRelease.findByPk(pressReleaseId);
        const article = await Article.findByPk(id);
        article.setPressRelease(pressRelease);
        return this.findById(article._id)
    }

    static async unlink(id) {
        const article = await Article.findByPk(id);
        article.setPressRelease(null);
        return this.findById(article._id)
    }

    static async set(id, body) {
      const { themes, pressReleaseId } = body;

      const transaction = await sequelize.transaction();

      try {
          const themesArticle = await ArticleTheme.findAndCountAll({ where: { _id: themes }});
          const pressRelease = await PressRelease.findOne({ where: { _id: pressReleaseId } });

          if(
              (themesArticle.count == themes.length)
          ) {
              const toAdd = await Article.findByPk(id);
              await toAdd.update({
                  _id: body._id,
                  title: body.title,
                  tone: body.tone,
                  date: body.date,
                  description: body.description
              }, {
                  transaction
              })

              if( pressReleaseId != undefined || pressReleaseId != null) {
                const pressRelease = await PressRelease.findOne({ where: { _id: pressReleaseId } });
                await toAdd.setPressRelease(pressRelease, {transaction});
              }

              await toAdd.setArticleThemes(themesArticle.rows, {transaction});

              await transaction.commit()

              return this.findById(toAdd._id);
          } else {
              throw Error("invalid themes or press release ID")
          }
      } catch(e) {
          await transaction.rollback();
          throw e;
      }
    }

    static async remove(id) {
        const toRemove = await Article.findByPk(id, ArticleFindingOptions);
        toRemove.destroy();
        return buildArticleObject(toRemove);
      }
}