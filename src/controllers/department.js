const sequelize = require("../db");

const Department = sequelize.models.Department;

module.exports.DepartmentController = class DepartmentController {
  static async findAll() {
    return Department.findAll({
      attributes: ["_id", "value"],
    });
  }

  static async findById(id) {
    return Department.findByPk(id);
  }

  static async add(body) {
    return Department.create(body);
  }

  static async set(id, body) {
    return Department.update(body, { where: { _id: id }, returning: true });
  }

  static async remove(id) {
    const toRemove = await Department.findByPk(id);
    if ((await toRemove.countMedia()) != 0) {
      throw Error("Can't remove this department : used by media")
    }
    toRemove.destroy();
    return toRemove;
  }
};
