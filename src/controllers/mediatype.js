const sequelize = require("../db");

const Mediatype = sequelize.models.Mediatype;

module.exports.MediatypeController = class MediatypeController {
  static async findAll() {
    return Mediatype.findAll({
      attributes: ["_id", "value"],
    });
  }

  static async findById(id) {
    return Mediatype.findByPk(id);
  }

  static async add(body) {
    return Mediatype.create(body);
  }

  static async set(id, body) {
    return Mediatype.update(body, { where: { _id: id }, returning: true });
  }

  static async remove(id) {
    const toRemove = await Mediatype.findByPk(id);
    if ((await toRemove.countMedia()) != 0) {
      throw Error("Can't remove this mediatype : used by media")
    }
    toRemove.destroy();
    return toRemove;
  }
};
