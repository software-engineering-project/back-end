const sequelize = require("../db");

const ArticleTheme = sequelize.models.ArticleTheme;

module.exports.ArticleThemeController = class ArticleThemeController {
  static async findAll() {
    return ArticleTheme.findAll({
      attributes: ["_id", "value"],
    });
  }

  static async findById(id) {
    return ArticleTheme.findByPk(id);
  }

  static async add(body) {
    return ArticleTheme.create(body);
  }

  static async set(id, body) {
    return ArticleTheme.update(body, { where: { _id: id }, returning: true });
  }

  static async remove(id) {
    const toRemove = await ArticleTheme.findByPk(id);
    if ((await toRemove.countArticles()) != 0) {
      throw Error("Can't remove this theme : used by some articles")
    }
    toRemove.destroy();
    return toRemove;
  }
};