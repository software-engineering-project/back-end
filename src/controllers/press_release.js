const { buildPressReleaseObject } = require("../builders/press_release");
const sequelize = require("../db");

const PressRelease = sequelize.models.PressRelease;
const PressReleaseType = sequelize.models.PressReleaseType;
const PressReleaseTheme = sequelize.models.PressReleaseTheme;
const Article = sequelize.models.Article;
const ArticleTheme = sequelize.models.ArticleTheme;


const PressReleaseFindingOptions = {
    include: [
        {
          model: PressReleaseTheme,
          through: { attributes: [] },
          attributes: ["_id", "value"],
        },
        {
          model: PressReleaseType,
          attributes: ["_id", "value"],
        },
        {
            model: Article,
            attributes: {
                exclude: ['createdAt', 'updatedAt']
            },
            include: [{
                model: ArticleTheme,
                attributes: {
                    exclude: ['createdAt', 'updatedAt']
                },
            }],
        }
      ],
      attributes: {
        exclude: ['createdAt', 'updatedAt']
    }
}
module.exports.PressReleaseController = class PressReleaseController {
    static async findAll() {
        return PressRelease.findAll(PressReleaseFindingOptions)
        .then((pressReleases) => {
            return pressReleases.map(buildPressReleaseObject)
        })
    }

    static async findById(id) {
        return PressRelease.findByPk(id, PressReleaseFindingOptions)
        .then(buildPressReleaseObject)
    }

    static async add(body) {
        const { themes, type, articles } = body;

        const transaction = await sequelize.transaction();

        try {
            const themesPR = await PressReleaseTheme.findAndCountAll({ where: { value: themes }});
            const typePR = await PressReleaseType.findOne({ where: { value: type } });
            const articlesPR = await Article.findAndCountAll({ where: { _id: articles } });

            if(
                (themesPR.count == themes.length) &&
                (typePR != undefined) && (typePr != null)
            ) {
                const toAdd = await PressRelease.create({
                    title: body.title,
                    date: body.date,
                    description: body.description
                }, {
                    transaction
                })

                await toAdd.setPressReleaseThemes(themesPR.rows, {transaction});
                await toAdd.setPressReleaseType(typePR, {transaction});
                
                if(articlesPR.count == articles.length()) {
                    const articlesPR = await Article.findAndCountAll({ where: { _id: articles } });
                    await toUpdate.setArticles(articlesPR.rows, {transaction});
                }

                await transaction.commit()

                return this.findById(toAdd._id);
            } else {
                throw Error("invalid type or articles IDs")
            }
        } catch(e) {
            await transaction.rollback();
            throw e;
        }
    }

    static async link(id, articles) {
        const articlesPR = await Article.findAll({where: { _id: articles }});
        const pressRelease = await PressRelease.findByPk(id);
        pressRelease.addArticles(articlesPR);
        return this.findById(pressRelease._id)
    }

    static async unlink(id, articles) {
        const articlesPR = await Article.findAll({where: { _id: articles }});
        const pressRelease = await PressRelease.findByPk(id);
        pressRelease.removeArticles(articlesPR);
        return this.findById(pressRelease._id)
    }

    static async set(id, body) {
        const { themes, type, articles } = body;

        const transaction = await sequelize.transaction();

        try {
            const themesPR = await PressReleaseTheme.findAndCountAll({ where: { _id: themes }});
            const typePR = await PressReleaseType.findOne({ where: { _id: type } });
            if(
                (themesPR.count == themes.length()) &&
                (typePR != undefined) && (typePr != null)
            ) {
                const toUpdate = await PressRelease.findByPk(id);

                await toUpdate.update({
                    _id: id,
                    title: body.title,
                    date: body.date,
                    description: body.description
                }, {
                    transaction
                })

                if(articlesPR.count == articles.length()) {
                    const articlesPR = await Article.findAndCountAll({ where: { _id: articles } });
                    await toUpdate.setArticles(articlesPR.rows, {transaction});
                }

                await toUpdate.setPressReleaseThemes(themesPR.rows, {transaction});
                await toUpdate.setPressReleaseType(typePR, {transaction});

                await transaction.commit()

                return this.findById(toUpdate._id);
            } else {
                throw Error("invalid type or articles IDs")
            }
        } catch(e) {
            await transaction.rollback();
            throw e;
        }
    }

    static async remove(id) {
        const toRemove = await PressRelease.findByPk(id, PressReleaseFindingOptions);
        toRemove.destroy();
        return buildPressReleaseObject(toRemove);
      }
}