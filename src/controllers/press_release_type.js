const sequelize = require("../db");

const PressReleaseType = sequelize.models.PressReleaseType;

module.exports.PressReleaseTypeController = class PressReleaseTypeController {
  static async findAll() {
    return PressReleaseType.findAll({
      attributes: ["_id", "value"],
    });
  }

  static async findById(id) {
    return PressReleaseType.findByPk(id);
  }

  static async add(body) {
    return PressReleaseType.create(body);
  }

  static async set(id, body) {
    return PressReleaseType.update(body, { where: { _id: id }, returning: true });
  }

  static async remove(id) {
    const toRemove = await PressReleaseType.findByPk(id);
    if ((await toRemove.countPressReleases()) != 0) {
      throw Error("Can't remove this press release type : used by some press releases")
    }
    toRemove.destroy();
    return toRemove;
  }
};
