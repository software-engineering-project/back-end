const sequelize = require("../db");

const PressReleaseTheme = sequelize.models.PressReleaseTheme;

module.exports.PressReleaseThemeController = class PressReleaseThemeController {
  static async findAll() {
    return PressReleaseTheme.findAll({
      attributes: ["_id", "value"],
    });
  }

  static async findById(id) {
    return PressReleaseTheme.findByPk(id);
  }

  static async add(body) {
    return PressReleaseTheme.create(body);
  }

  static async set(id, body) {
    return PressReleaseTheme.update(body, { where: { _id: id }, returning: true });
  }

  static async remove(id) {
    const toRemove = await PressReleaseTheme.findByPk(id);
    if ((await toRemove.countArticles()) != 0) {
      throw Error("Can't remove this theme : used by some press releases")
    }
    toRemove.destroy();
    return toRemove;
  }
};