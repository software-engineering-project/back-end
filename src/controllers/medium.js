const { buildMediumObject } = require("../builders/medium");
const sequelize = require("../db");

const Department = sequelize.models.Department;
const Media = sequelize.models.Media;
const Mediatype = sequelize.models.Mediatype;

const MediaFindingOptions = {
  include: [
    {
      model: Department,
      through: { attributes: [] },
      attributes: ["_id", "value"],
    },
    {
      model: Mediatype,
      attributes: ["_id", "value"],
    },
  ],
  attributes: {
    include: ["_id", "name"],
    exclude: ["createdAt", "updatedAt"],
  },
  order: [["name", "ASC"]],
};

module.exports.MediaController = class MediaController {
  static async findAll() {
    return Media.findAll(MediaFindingOptions).then((media) =>
      media.map(buildMediumObject)
    );
  }

  static async findById(id) {
    return Media.findByPk(id, MediaFindingOptions).then(buildMediumObject);
  }

  static async add(body) {
    const { departments, type } = body;

    await validateDepartments(departments);
    await validateMediatype(type);

    const transaction = await sequelize.transaction();

    try {
      const toAdd = await Media.create(
        {
          name: body.name,
          link: body.link,
          description: body.description,
        },
        { transaction }
      );

      const departmentModels = await Department.findAll(
        {
          where: {
            value: body.departments,
          },
        },
        { transaction }
      );

      const typeModel = await Mediatype.findOne(
        {
          where: {
            value: body.type,
          },
        },
        { transaction }
      );

      await toAdd.setDepartments(departmentModels, { transaction });
      await toAdd.setMediatype(typeModel, { transaction });

      await transaction.commit();

      return this.findById(toAdd._id);
    } catch (e) {
      await transaction.rollback();
      throw e;
    }
  }

  static async set(id, body) {
    const { type, departments } = body;

    await validateDepartments(departments);
    await validateMediatype(type);

    const transaction = await sequelize.transaction();

    try {
      const toUpdate = await Media.findByPk(id);

      await toUpdate.update(
        {
          _id: body._id,
          name: body.name,
          link: body.link,
          description: body.description,
        },
        { transaction }
      );

      await toUpdate.setDepartments([], { transaction });

      const departmentModels = await Department.findAll(
        {
          where: {
            value: body.departments,
          },
        },
        { transaction }
      );

      toUpdate.setDepartments(departmentModels, { transaction });

      const typeModel = await Mediatype.findOne(
        {
          where: {
            value: body.type,
          },
        },
        { transaction }
      );

      await toUpdate.setMediatype(typeModel, { transaction });

      await transaction.commit();

      return this.findById(toUpdate._id);
    } catch (e) {
      await transaction.rollback();
      throw e;
    }
  }

  static async remove(id) {
    const toRemove = await Media.findByPk(id, MediaFindingOptions);
    toRemove.destroy();
    return buildMediumObject(toRemove);
  }
};

async function validateMediatype(type) {
  const mediatypesFromQuery = (await Mediatype.findAll()).map((m) => {
    return m.value;
  });
  const areRequestMediatypeValid = mediatypesFromQuery.includes(type);

  if (!areRequestMediatypeValid) {
    throw Error("Invalid mediatype");
  }
}

async function validateDepartments(departments) {
  const departmentsFromQuery = (await Department.findAll()).map((d) => {
    return d.value;
  });
  const areRequestDepartmentsValid = departments.every((department) => {
    return departmentsFromQuery.includes(department);
  });

  if (!areRequestDepartmentsValid) {
    throw Error("Invalid department");
  }
}
